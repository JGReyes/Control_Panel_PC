program ControlPanelV3;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, LazSerialPort, lazmouseandkeyinput, captura, teclado, cpanel;

{$R *.res}


begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmCaptura, frmCaptura);
  Application.Run;
end.

