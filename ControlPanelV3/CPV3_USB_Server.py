import socket
import sys
import threading
import pywinusb.hid as hid
from time import sleep
from asyncio.locks import Lock
import logging

class Cpanel(threading.Thread):
    ''' Representa el panel de control.

    Se encarga de conectar por USB con el panel y
    de mandar los comandos recibidos al programa mediante TCP/IP
    '''

    def __init__(self, direccion, puerto_salida):
        ''' Inicializa el panel.
        
        Args:
            dirección:     Dirección IP para el socket de escucha.
            puerto_salida: Puerto de escucha.
        '''
        
        self.sock_o = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock_i = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    
        self.vendor_id = 0x1234
        self.prouct_id = 0x1235
        self.panel = None           #Representa el socket de conexión con el panel.
        self.usb_conectado = False  # Estado de la conexión.
        self.sock_in = None
        self.sock_out = None
        self.lock = Lock()
        self.direccion = direccion
        self.puerto_salida = puerto_salida
        threading.Thread.__init__(self)

    def run(self):
        ''' Inicia el servidor y tras la conexión del cliente se encarga
        de recibir datos.
        
        En caso de fallo en el socket, se pide al cliente la reconexión y
        se pone a la escucha de nuevo.
        
        Utiliza dos sockets; uno para recepción y otro para transmisión.
        '''
        
        datos=[0x00]*6
        puerto_entrada = self.puerto_salida + 1
        buf_datos = bytearray(datos)
        try:
            self.sock_o.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock_i.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock_o.bind((self.direccion, self.puerto_salida))
            self.sock_o.listen(1)
            (sock_cliente, sock_direccion) = self.sock_o.accept()
            self.sock_out = sock_cliente
            self.sock_out.settimeout(None)
            
            self.sock_i.bind((self.direccion, puerto_entrada))
            self.sock_i.listen(1)
            (sock_cliente, sock_direccion) = self.sock_i.accept()
            self.sock_in = sock_cliente
            self.sock_in.settimeout(None)
            
            while True:
                try:
                    self.sock_in.recv_into(buf_datos, 6)  
                    if buf_datos[0] == 0xcc: #Cierra el programa.
                        self.cierra_sockets()
                        sys.exit(0) 
                    elif buf_datos[0] != 0: #Manda los datos al panel.
                        self.__enviar_hid(buf_datos)    
                except:
                    if self.sock_in:
                        self.sock_in.close()
                    #Indica al PC que se reconecte al socket de salida.
                    self.__enviar_datos((0xa1,)) 
                    self.sock_i.listen(1)
                    (sock_cliente, sock_direccion) = self.sock_i.accept()
                    self.sock_in = sock_cliente
                    self.sock_in.settimeout(None)    
                    
        except:
            self.cierra_sockets()
            #logging.exception('Error en función run de la clase Cpanel.')
    
    def cierra_sockets(self):
        ''' Cierra todos los sockets que se han utilizado.'''
        
        if self.sock_in:
            self.sock_in.close()
        if self.sock_out:
            self.sock_out.close()
        if self.sock_i:
            self.sock_i.close() 
        if self.sock_o:
            self.sock_o.close()     
            
    def conectar_panel(self):  
        ''' Realiza la conexión USB con el panel.'''
                          
        filtro = hid.HidDeviceFilter(vendor_id=self.vendor_id, product_id \
                                         =self.prouct_id) 
        dispositivo_hid = filtro.get_devices()
        if dispositivo_hid:
            self.panel = dispositivo_hid[0]
            self.panel.set_raw_data_handler(self.__leer_hid)
            self.panel.open()
            self.usb_conectado = True
        else:
            self.usb_conectado = False
            
    def __enviar_hid(self, datos):
        ''' Envía datos por USB.
        
        Arg:
            datos: Datos a mandar. Puede ser una lista,
            tupla o un bytearray de 6 bytes como máximo.
        '''
        
        informe = self.panel.find_output_reports()
        buffer = [0x00]*9
        bbuffer = bytearray(buffer)
        
        for i in range(0, 6):
            bbuffer[i+1] = datos[i]
        
        informe[0].set_raw_data(bbuffer)
        informe[0].send()
        
    def __leer_hid(self, datos):
        ''' Envia los datos leidos al programa principal.
        
        Arg:
            datos: Datos recibidos.
            
        Nota:
            Se llama automáticamente a está función al recibir los datos.
        '''
        
        buffer_entrada = [0x00]*6
        buffer = bytearray(buffer_entrada)
        for i in range(0, 6):
            buffer[i] = datos[i+1]   
        self.__enviar_datos(buffer)
            
    def __enviar_datos(self, datos):
        ''' Envía datos al programa de PC.
        
        Arg:
            datos: Datos a mandar. Puede ser una lista,
            tupla o un bytearray de 6 bytes como máximo.
        '''
        
        if isinstance(datos, bytearray):
            buffer = datos
        else:
            tamano = len(datos)
            if tamano > 6:
                tamano = 6
            buffer_list = [0x00]*6
            buffer = bytearray(buffer_list)
            for i in range(0, tamano):
                buffer[i] = datos[i]
            
        if buffer[0] != 0x00:    # Solo los manda si el primer elemento no es 0.
            self.lock.acquire() 
            self.sock_out.sendall(buffer)
            if self.lock.locked():
                self.lock.release()  
                
class compruebaUSB(threading.Thread):
    ''' Se encarga de comprobar si el panel USB está conectado 
    gracias a la monitorización de los hilos en ejecución.'''
    
    def run(self):
        while True:
            sleep(1)
            nhilos = len(threading.enumerate())
            if nhilos != 5: # 8 cuando se utiliza el debugger de PyDev
                logging.info('Panel USB desconectado.')
                sys.exit(0)
            
    
if __name__ == '__main__':
    logging.basicConfig(filename='ControlPanelV3_Log.log', level=logging.INFO,\
                        format='%(asctime)s %(message)s', 
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        filemode='w')
    argumentos = sys.argv
    if len(argumentos) != 3:
        logging.error('Número de argumentos erróneo. Indique dirección IP y puerto.\
        Ej: 127.0.0.1 12345')
    else:
        try:
            logging.info('Iniciando servidor para ControlPanelV3 (USB)...')
            panel = Cpanel(argumentos[1], int(argumentos[2]))
            while panel.usb_conectado != True:
                panel.conectar_panel()
                sleep(1)
            logging.info('Panel USB detectado.Ok')
            panel.setDaemon(True)
            con_USB = compruebaUSB()
            con_USB.setDaemon(True)
            panel.start()
            sleep(0.1)
            con_USB.start()
            logging.info('Servidor iniciado correctamente.Ok')
            # Si se ha desconectado el panel USB se cierra la aplicación.
            con_USB.join()  
            panel.cierra_sockets()
            sys.exit(0)  
        except:
            #logging.exception('Error en main.')
            pass
 
      
        
           
            
        
        
        
