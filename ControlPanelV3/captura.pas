{
 /*
 *  captura
 *
 *  Created on: 31/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */   

}

unit captura;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLType, IniFiles, Types;

type

  { TfrmCaptura }

  TfrmCaptura = class(TForm)
    btnCapturar: TButton;
    btnCerrar: TButton;
    ckbRotatorios: TCheckBox;
    cmbTipo: TComboBox;
    gbTipo: TGroupBox;
    lblComando: TLabel;
    lblCodigo: TLabel;
    lblComando2: TLabel;
    lblTeclas: TLabel;
    lblPunto: TLabel;
    lblTeclas2: TLabel;
    lblPunto2: TLabel;
    {Inicia la captura de teclas y las puestra en pantalla.}
    procedure btnCapturarClick(Sender: TObject);
    {Captura las teclas de cambio. Ctrl, Alt y Shift.
    @param(Key Contiene la tecla pulsada.)
    @param(Shift Indica si es una tecla de control.)}
    procedure btnCapturarKeyDown(Sender: TObject; var Key: word;
      Shift: TShiftState);
    {Captura las teclas alfabéticas.
    @param(Key Es la tecla pulsada en forma de carácter.)}
    procedure btnCapturarKeyPress(Sender: TObject; var Key: char);
    {Captura el resto de teclas del teclado.
    @param(Key Es la tecla pulsada.)
    @param(Shift Indica si es una tecla de control.)}
    procedure btnCapturarKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    {Permite introducir la tecla enter del teclado numérico, ya que en los
    teclados sin numpad, hay que recurrir al teclado virtual de windows y en
    éste la tecla enter del numérico comparte el código con el enter del teclado.}
    procedure btnCapturarMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    {Se encarga de guardar las teclas capturadas en el archivo de perfil y
    de cerrar el formulario.}
    procedure btnCerrarClick(Sender: TObject);
    {Al cambiar el estado del checkbox, se actualiza el valor de variable que
    indica si está o no el modo av/ret de los rotatorios activado.
    También guarda el valor en el archivo de perfil.}
    procedure ckbRotatoriosChange(Sender: TObject);
    {Al cerrar el formulario de captura quita los puntos rojos del formulario
    principal.}
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    { Al abrir el formulario de captura muestra los puntos rojos en el
    formulario principal.}
    procedure FormShow(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
    Comando: string; //< Almacena el comando capturado por el formulario principal.
  end;

{Se encarga de mostrar y ocultar los puntos rojos en el modo de edicicón.
@param(mostrar Si se pone a @true muestra los puntos en la pantalla.
Si se pone a @false se quitan los puntos de la pantalla.
Por defecto vale @true)}
procedure mostrarPuntos(mostrar: boolean = True);

var
  frmCaptura: TfrmCaptura;       //< Formulario de carptura de teclas.
  capturar: boolean = False;     //< Indica si se deben capturar las teclas.
  {Indica si se ha introducido el código de un tecla, por lo que no captura
  más teclas.}
  codigoIntro: boolean = False;

implementation

uses
  main;


{$R *.lfm}

{ TfrmCaptura }


procedure TfrmCaptura.btnCapturarClick(Sender: TObject);
begin
  if (capturar = False) then
  begin
    capturar := True;
    btnCapturar.Caption := 'Capturando...';
    lblTeclas2.Caption := '';
    lblCodigo.Caption := '';
    codigoIntro := False;

  end
  else
  begin
    capturar := False;
    btnCapturar.Caption := 'Iniciar/parar captura';

  end;

end;

procedure TfrmCaptura.btnCapturarKeyDown(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  ctrl: string;
  shf: string;
  alt: string;
  teclas_bloq: array [0..7] of Word = (13, 32, 37, 38, 39, 40, 121, 123);
  i: Integer = 0;
begin

  if (ssShift in Shift) then
    shf := 'Shift+';

  if (ssCtrl in Shift) then
    ctrl := 'Ctrl+';

  if (ssAlt in Shift) then
    alt := 'Alt+';



  if (capturar = True) then
  begin

    if (Pos(alt, lblTeclas2.Caption) = 0) then
      lblTeclas2.Caption := alt + lblTeclas2.Caption;

    if (Pos(ctrl, lblTeclas2.Caption) = 0) then
      lblTeclas2.Caption := ctrl + lblTeclas2.Caption;

    if (Pos(shf, lblTeclas2.Caption) = 0) then
      lblTeclas2.Caption := shf + lblTeclas2.Caption;

    // Desabilito tecla enter , espacio, las flechas de dirección
    //y teclas F10 y F12 para no activar el botón.
    for i:= 0 to High(teclas_bloq) do
      if teclas_bloq[i] = Key then
        Key := $0;

  end;
end;

procedure TfrmCaptura.btnCapturarKeyPress(Sender: TObject; var Key: char);
begin

  if (capturar = True) then
  begin
    if Key in ['A'..'Z', 'a'..'z'] then
    begin
      // Solo se añade la tecla si no está repetida.
      if (codigoIntro = False) then
      begin
        lblTeclas2.Caption := lblTeclas2.Caption + upCase(Key);
        lblCodigo.Caption := lblCodigo.Caption + IntToStr(Ord(upcase(Key)));
        codigoIntro := True;
      end;
    end;
  end;

end;

procedure TfrmCaptura.btnCapturarKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
var
  k: string = '';
begin

  if (capturar = True) then
  begin
    case Key of
      VK_0: k:= '0';
      VK_1: k:= '1';
      VK_2: k:= '2';
      VK_3: k:= '3';
      VK_4: k:= '4';
      VK_5: k:= '5';
      VK_6: k:= '6';
      VK_7: k:= '7';
      VK_8: k:= '8';
      VK_9: k:= '9';
      VK_ADD: k := 'ADD';
      VK_BACK: k := 'BACK';
      VK_CAPITAL: k := 'CAPITAL';
      VK_DECIMAL: k := 'DECIMAL';
      VK_DELETE: k := 'DELETE';
      VK_DIVIDE: k := 'DIVIDE';
      VK_DOWN: k := 'DOWN';
      VK_END: k := 'END';
      VK_ESCAPE: k := 'ESCAPE';
      VK_F1: k := 'F1';
      VK_F2: k := 'F2';
      VK_F3: k := 'F3';
      VK_F4: k := 'F4';
      VK_F5: k := 'F5';
      VK_F6: k := 'F6';
      VK_F7: k := 'F7';
      VK_F8: k := 'F8';
      VK_F9: k := 'F9';
      VK_F10: k := 'F10';
      VK_F11: k := 'F11';
      VK_F12: k := 'F12';
      VK_FINAL: k := 'FINAL';
      VK_HOME: k := 'HOME';
      VK_INSERT: k := 'INSERT';
      VK_NUMPAD0: k := 'NUMPAD0';
      VK_NUMPAD1: k := 'NUMPAD1';
      VK_NUMPAD2: k := 'NUMPAD2';
      VK_NUMPAD3: k := 'NUMPAD3';
      VK_NUMPAD4: k := 'NUMPAD4';
      VK_NUMPAD5: k := 'NUMPAD5';
      VK_NUMPAD6: k := 'NUMPAD6';
      VK_NUMPAD7: k := 'NUMPAD7';
      VK_NUMPAD8: k := 'NUMPAD8';
      VK_NUMPAD9: k := 'NUMPAD9';
      VK_MULTIPLY: k := 'MULTIPLY';
      VK_SPACE: k := 'SPACE';
      VK_SEPARATOR: k := 'SEPARATOR';
      VK_SUBTRACT: k := 'SUBTRACT';
      VK_TAB: k := 'TAB';
      VK_UP: k := 'UP';
      VK_RIGHT: k := 'RIGHT';
      VK_LEFT: k := 'LEFT';
      VK_PAUSE: k := 'PAUSE';
      VK_SCROLL: k := 'SCROLL';
      VK_RETURN: k := 'RETURN';
      VK_NUMLOCK: k := 'NUMLOCK';
      VK_PRINT: k := 'PRINT';
      VK_PRIOR: k := 'PRIOR';
      220: k := 'º';
      226: k := '<';
      176: k := 'NUMPAD_ENTER';
      VK_NEXT: k := 'NEXT';
      VK_LCL_CLOSE_BRAKET: k := 'CLOSE_BRAKET';
      VK_LCL_COMMA: k := 'COMMA';
      VK_LCL_EQUAL: k := 'EQUAL';
      VK_LCL_MINUS: k := 'MINUS';
      VK_LCL_POINT: k := 'POINT';
      VK_LCL_POWER: k := 'POWER';
      VK_LCL_QUOTE: k := 'QUOTE';
      VK_LCL_SEMI_COMMA: k := 'SEMI_COMMA';
      VK_LCL_OPEN_BRAKET: k := 'OPEN_BRAKET';
      VK_LCL_SLASH: k := 'SLASH';
      VK_LCL_TILDE: k := 'TILDE';

    end;

    if (k <> '') and (codigoIntro = False) then
    begin
      lblTeclas2.Caption := lblTeclas2.Caption + k;
      lblCodigo.Caption := IntToStr(Key);
      codigoIntro := True;
    end;

    // Desabilito la tecla Alt para no mandarla a la aplicación.
    if (Key = 18) then
      Key := $0;
  end;

end;

procedure TfrmCaptura.btnCapturarMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  if (btnCapturar.Caption = 'Capturando...') and (codigoIntro = false) then
  begin
     lblTeclas2.Caption := lblTeclas2.Caption + 'NUMPAD_ENTER+';
     lblCodigo.Caption:= '176';
     codigoIntro := True;
  end;

end;

procedure TfrmCaptura.btnCerrarClick(Sender: TObject);
begin
  if (btnCapturar.Caption = 'Capturando...') then
    btnCapturar.Click;

  mostrarPuntos(False);
  try

    // Guarda la configuración del comando en al archivo de perfil.
    perfil.WriteString(Comando, 'teclas', lblTeclas2.Caption);
    perfil.WriteString(Comando, 'codigo', lblCodigo.Caption);
    perfil.WriteString(Comando, 'tipo', cmbTipo.Caption);
    perfil.WriteString(Comando, 'punto', lblPunto2.Caption)

  except
    on E: Exception do
    begin
      perfil.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;
  end;
  Close;

end;

procedure TfrmCaptura.ckbRotatoriosChange(Sender: TObject);
begin
  try

    if (ckbRotatorios.Checked) then
    begin
      perfil.WriteString('ROTATORIOS', 'Modo_av/ret', 'Activado');
      modo_av_ret_rotatorios := True;
    end
    else
    begin
      perfil.WriteString('ROTATORIOS', 'Modo_av/ret', 'Desactivado');
      modo_av_ret_rotatorios := False;
    end;
  except
    on E: Exception do
    begin
      perfil.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TfrmCaptura.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  mostrarPuntos(False);
end;

procedure mostrarPuntos(mostrar: boolean = True);
var
  vControl: integer;
  nombre: string;
begin
  for vControl := 0 to Form1.Panel1.ControlCount - 1 do
  begin
    nombre := Form1.Panel1.Controls[vControl].Name;
    if (Pos('sp', nombre) > 0) then    // Si el nombre contiene la cadena sp.
      if mostrar then
        Form1.Panel1.Controls[vControl].Visible := True
      else
        Form1.Panel1.Controls[vControl].Visible := False;
  end;
end;

procedure TfrmCaptura.FormShow(Sender: TObject);
begin
  mostrarPuntos();
end;

end.



