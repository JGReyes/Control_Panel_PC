unit teclado;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLType, JwaWinUser, Keyboard;

type

  {Clase encargada de emular un teclado.
   Manda scancodes, lo que lo hace indistinguible de una teclado real,
   siendo el único método que funciona en aplicaciones DirectX.}
  Tteclado = class

    {Realiza la misma función que presionar una tecla.
    @param(tecla Es el código de la tecla que se quiere emular.)}
    procedure pulsar(tecla:Word);
    {Realiza la misma función que soltar una tecla.
    @param(tecla Es el código de la tecla que se quiere emular.)}
    procedure soltar(tecla:Word);

  private
    {Se encarga que obtener el scancode del códgio de tecla proporcionado.
    @param(tecla Es el código de la tecla que se quiere emular.)
    @returns(El scancode del de la tecla pasada como parámetro. De no
    encontrar la tecla, devuelve 0)}
    function conv_codigo_hw(tecla: Word ): Word;
  public


  end;

codigosHW = array[0..99, 0..1] of Word; //< Contiene los scancodes (Set 1)

var
   {En la primera columna se tienen los codigos o virtual key-codes que identifican
   una tecla en el sistema operativo. En la segunda columna se tienen los
   scancodes, los codigos que envían los drivers de los teclados al sistema
   operativo.}
   codigos: codigosHW=(
     (VK_SHIFT, 42),
     (VK_CONTROL, 29),
     (VK_MENU, 56),
     (VK_0, 11),
     (VK_1, 2),
     (VK_2, 3),
     (VK_3, 4),
     (VK_4, 5),
     (VK_5, 6),
     (VK_6, 7),
     (VK_7, 8),
     (VK_8, 9),
     (VK_9, 10),
     (VK_A, 30),
     (VK_B, 48),
     (VK_C, 46),
     (VK_D, 32),
     (VK_E, 18),
     (VK_F, 33),
     (VK_G, 34),
     (VK_H, 35),
     (VK_I, 23),
     (VK_J, 36),
     (VK_K, 37),
     (VK_L, 38),
     (VK_M, 50),
     (VK_N, 49),
     (VK_O, 24),
     (VK_P, 25),
     (VK_Q, 16),
     (VK_R, 19),
     (VK_S, 31),
     (VK_T, 20),
     (VK_U, 22),
     (VK_V, 47),
     (VK_W, 17),
     (VK_X, 45),
     (VK_Y, 21),
     (VK_Z, 44),
     (VK_ADD, 78),
     (VK_BACK, 14),
     (VK_CAPITAL, 58),
     (VK_DECIMAL, 83),
     (VK_DELETE, 1211),   // Le sumo 1000 a los scancodes expandidos.
     (VK_DIVIDE, 1181),
     (VK_DOWN, 1208),
     (VK_END,  1207),
     (VK_ESCAPE, 1),
     (VK_F1, 59),
     (VK_F2, 60),
     (VK_F3, 61),
     (VK_F4, 62),
     (VK_F5, 63),
     (VK_F6, 64),
     (VK_F7, 65),
     (VK_F8, 66),
     (VK_F9, 67),
     (VK_F10, 68),
     (VK_F11, 87),
     (VK_F12, 88),
     (VK_FINAL, 1207),
     (VK_HOME,  1199),
     (VK_INSERT, 1210),
     (VK_NUMPAD0, 82),
     (VK_NUMPAD1, 79),
     (VK_NUMPAD2, 80),
     (VK_NUMPAD3, 81),
     (VK_NUMPAD4, 75),
     (VK_NUMPAD5, 76),
     (VK_NUMPAD6, 77),
     (VK_NUMPAD7, 71),
     (VK_NUMPAD8, 72),
     (VK_NUMPAD9, 73),
     (VK_MULTIPLY, 55),
     (VK_SPACE, 57),
     (VK_SEPARATOR, 1053),
     (VK_SUBTRACT, 74),
     (VK_TAB, 15),
     (VK_UP, 1200),
     (VK_RIGHT, 1205),
     (VK_LEFT, 1203),
     (VK_PAUSE, 1197),
     (VK_SCROLL, 1070),
     (VK_RETURN, 28),
     (VK_NUMLOCK, 1069),
     (VK_PRIOR, 1201),
     (VK_OEM_5, 41),
     (VK_OEM_102, 86),
     (VK_NEXT, 1209),
     (VK_LCL_CLOSE_BRAKET, 13),
     (VK_LCL_COMMA, 51),
     (VK_LCL_EQUAL, 27),
     (VK_LCL_MINUS, 53),
     (VK_LCL_POINT, 52),
     (VK_LCL_QUOTE, 40),
     (VK_LCL_SEMI_COMMA, 26),
     (VK_LCL_OPEN_BRAKET, 12),
     (VK_LCL_SLASH, 43),
     (VK_LCL_TILDE, 39),
     (176, 1028)  // Num Return.
    );


implementation

{ teclado }

procedure Tteclado.pulsar(tecla: Word);
var
   Input: TInput;
begin
   FillChar(Input, SizeOf(Input), 0);
   Input.type_ := INPUT_KEYBOARD;
   Input.ki.wScan := conv_codigo_hw(tecla);
   Input.ki.dwFlags := KEYEVENTF_SCANCODE;

   if (Input.ki.wScan > 1000) then
   begin
     Input.ki.wScan := Input.ki.wScan - 1000;
     Input.ki.dwFlags := (Input.ki.dwFlags or KEYEVENTF_EXTENDEDKEY);
   end;

   SendInput(1, @Input, SizeOf(Input));
end;

procedure Tteclado.soltar(tecla: Word);
var
   Input: TInput;
begin
   FillChar(Input, SizeOf(Input), 0);
   Input.type_ := INPUT_KEYBOARD;
   Input.ki.wScan := conv_codigo_hw(tecla);
   Input.ki.dwFlags := (KEYEVENTF_SCANCODE or KEYEVENTF_KEYUP);

   if (Input.ki.wScan > 1000) then
   begin
     Input.ki.wScan := Input.ki.wScan - 1000;
     Input.ki.dwFlags := (Input.ki.dwFlags or KEYEVENTF_EXTENDEDKEY);
   end;

   SendInput(1, @Input, SizeOf(Input));
end;

function Tteclado.conv_codigo_hw(tecla: Word): Word;
var
   i: Integer = 0;
begin
  for i:= 0 to High(codigos) do
  begin
    if (tecla = codigos[i][0]) then
      Exit(codigos[i][1]);
  end;

  Exit(0);
end;



end.

