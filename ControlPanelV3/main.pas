{ <description>

 /*
 *  main
 *
 *  Created on: 27/10/2016
 *      Author: JGReyes (www.circuiteando.net)
 *
 *  License: GPLv3 (www.gnu.org/licenses/gpl.html)
 */
}



unit main;

{$mode objfpc}{$H+}


interface

uses
  Classes, SysUtils, LazFileUtils, LazSerial, ECGroupCtrls, Forms,
  Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, dateutils,
  IniFiles, LCLType, Types, teclado, cpanel;

type

  { TForm1 }

  {Array de bytes que contiene las tramas a enviar y recibir del dispositivo.}
  TBytesRec = array [0..5] of byte;
  {@value edicion Activa el modo edición, permitiendo asirnar comandos de teclas
   a los botones/interruptores del panel.
   @value prueba En este modo, al pulsar/activar un botón/interruptor, un punto
   de color rojo se muestra en pantalla en la posición correspondiente durante
   medio segundo.
   @value mormal Éste es el modo habitual de operación, en él no se muestra nada
   en pantalla, siendo el único en el que se mandan las teclas asiganadas al
   sistema operativo.}
  TModo = (edicion, prueba, normal);


  TForm1 = class(TForm)
    btnConfig: TButton;
    btnCargar: TButton;
    gbSerie: TGroupBox;
    gbEstado: TGroupBox;
    imgExterno: TImage;
    imgDerecha: TImage;
    imgIzquierda: TImage;
    lblNombre: TLabel;
    lblCon: TLabel;
    lblcon2: TLabel;
    lblPerfil2: TLabel;
    lblPerfil: TLabel;
    lblmodo2: TLabel;
    lblModo: TLabel;
    OpenDlg: TOpenDialog;
    Panel1: TPanel;
    Panel2: TPanel;
    rgConexion: TECRadioGroup;
    rgModo: TECRadioGroup;
    Serial: TLazSerial;
    sp1: TShape;
    sp10: TShape;
    sp11: TShape;
    sp12: TShape;
    sp13: TShape;
    sp14: TShape;
    sp15: TShape;
    sp16: TShape;
    sp17: TShape;
    sp18: TShape;
    sp19: TShape;
    sp2: TShape;
    sp20: TShape;
    sp21: TShape;
    sp22: TShape;
    sp23: TShape;
    sp24: TShape;
    sp25: TShape;
    sp26: TShape;
    sp27: TShape;
    sp28: TShape;
    sp29: TShape;
    sp3: TShape;
    sp30: TShape;
    sp31: TShape;
    sp32: TShape;
    sp33: TShape;
    sp34: TShape;
    sp35: TShape;
    sp36: TShape;
    sp37: TShape;
    sp38: TShape;
    sp39: TShape;
    sp4: TShape;
    sp40: TShape;
    sp41: TShape;
    sp42: TShape;
    sp43: TShape;
    sp44: TShape;
    sp45: TShape;
    sp46: TShape;
    sp47: TShape;
    sp48: TShape;
    sp49: TShape;
    sp5: TShape;
    sp50: TShape;
    sp51: TShape;
    sp52: TShape;
    sp53: TShape;
    sp54: TShape;
    sp55: TShape;
    sp56: TShape;
    sp57: TShape;
    sp58: TShape;
    sp59: TShape;
    sp6: TShape;
    sp60: TShape;
    sp61: TShape;
    sp62: TShape;
    sp63: TShape;
    sp64: TShape;
    sp65: TShape;
    sp66: TShape;
    sp67: TShape;
    sp68: TShape;
    sp69: TShape;
    sp7: TShape;
    sp70: TShape;
    sp71: TShape;
    sp72: TShape;
    sp73: TShape;
    sp74: TShape;
    sp75: TShape;
    sp76: TShape;
    sp77: TShape;
    sp78: TShape;
    sp79: TShape;
    sp8: TShape;
    sp80: TShape;
    sp81: TShape;
    sp82: TShape;
    sp83: TShape;
    sp84: TShape;
    sp85: TShape;
    sp86: TShape;
    sp87: TShape;
    sp88: TShape;
    sp89: TShape;
    sp9: TShape;
    sp90: TShape;
    sp91: TShape;
    sp92: TShape;
    sp93: TShape;
    Splitter2: TSplitter;
    {Comprueba periódicamente si se han recibido datos y si el panel sigue
    conectado.}
    tmrDatosRecibidos: TTimer;
    {Carga un archivo de perfil o crea uno nuevo si se indica.}
    procedure btnCargarClick(Sender: TObject);
    {Permite configurar los parámetros del puerto serie.}
    procedure btnConfigClick(Sender: TObject);
    {Inicializa ciertas variables a crearse el formulario.}
    procedure FormCreate(Sender: TObject);
    {Libera la memoria del objeto Tinifile si se encontrara asiganada
    al cerrar la aplicación.}
    procedure FormDestroy(Sender: TObject);
    {Permine conectar y desconectar la comunicación con el dispositivo.}
    procedure rgConexionSelectionChange(Sender: TObject);
    {Cambia los modos de operación del programa.}
    procedure rgModoSelectionChange(Sender: TObject);
    {Procedimiento que se ejecuta al recibir datos por el puerto serie.
    En caso de error en los datos, se envía un comando al dispositivo para que
    éste reenvíe la información.}
    procedure SerialRxData(Sender: TObject);
    {Captura el nombre del punto rojo para asociarlo con el botón/interruptor
    correspondiente. Para ello se ha de mover la rueda del ratón encima del punto
    deseado.}
    procedure sp1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: integer; MousePos: TPoint; var Handled: boolean);
    {Comprueba cada 20 ms si se han rebido dados del puerto USB y llama
     a la función datos_usb_rec en caso afirmativo.}
    procedure tmrDatosRecibidosTimer(Sender: TObject);
    {Se encarga de conectar con el panel}
    procedure conectar_panel();
    {Se encarga de desconectar el panel.}
    procedure desconectar_panel();


  private
    { private declarations }
  public
    { public declarations }
  end;

{Se encarga de cargar una trama en el buffer de salida para comunicarse con
el dispositivo.
@param(trama_buffer es un array donde se guardan los datos a transmitir.)
@param(Comando es el comando en hexadecimal que se quiere enviar el dispositivo.}
procedure preparar_trama(var trama_buffer: TBytesRec; Comando: cardinal);
{Lee una trama recibida y la decodifica.
@param(trama_buffer Es un array que contiene la trama a decodificar.)
@returns(Número que representa el interruptor/botón accionado,
0 en caso de trama nó válida por falta de byte de sincronización o por crc
incorrecto.)}
function leer_trama(const trama_buffer: TBytesRec): cardinal;
{Calcula el crc16 de los datos a enviar o recibidos.
@param(CRC es la variable donde se guarda el valor calculado.)
@param(Data es el byte de información sobre el que se calcula el crc.)}
procedure CRC16_Update(var CRC: word; Data: byte);
{Carga un perfil de teclas en el programa.
@param(ruta es la ruta completa del archivo a cargar. Tiene extensión .cp3)}
procedure carga_perfil(ruta: string);
{Crea un archivo y perfil nuevo si no existe el indicado en el diálogo de
selección, para posteriormente poder editarlo.
@param(ruta es la ruta completa del archivo a crear. Tiene extensión .cp3)}
procedure crear_perfil(ruta: string);
{Se encarga de procesar el comando enviado por el dispositivo y pulsar la tecla
correspondiente o configurarla, según el modo de operación que esté seleccionado.
@param(Comando es una cadena de texto que contiene el botón/interruptor
accionado en el dispositivo.)
@returns(1 si no se ha mandado la tecla o no se ha procesado el comando.)}
function teclado_pulsar(Comando: string): integer;
{Lee la configuración del comando recibido y carga las teclas que debe emular.
@param(Comando Es el comando recibido del dispositivo.)
@param(tipo Es la acción que se realiza con la tecla. Puede ser:
---Pulsar: emula una pulsación rápida de la tecla.
---Mantener: emula el mantener una tecla continuamente pulsada.
---Soltar: emula que se suelta una tecla mantenida anteriormente.)
@param(punto Indica el nombre del punto rojo que se asocia al botón/interruptor
para poder comprobar en el modo Prueba que se esta reconociendo el mismo.)
@param(teclas Es una cadena de texto donde se encuentra la secuencia de teclas a
emular separadas por el signo +.)
@param(codigo Representa el código de la tecla pulsada que no es una tecla de
control, para emular correctamente su pulsación en el sistema operativo.)
@param(mostrar Indica si se quiere carga los datos anteriores en el formulario
de edición. Por defecto es @false)
@returns(@true si el comando existe en el perfil, @false en caso contrario.)}
function cargar_teclas(var Comando: string; var tipo: string; var punto: string;
  var teclas: string; var codigo: word; mostrar: boolean = False):Boolean;
{Manda las teclas al sistema operativo mediante llamadas a su API.
@param(tipo Es la acción que se realiza con la tecla. Puede ser:
---Pulsar: emula una pulsación rápida de la tecla.
---Mantener: emula el mantener una tecla continuamente pulsada.
---Soltar: emula que se suelta una tecla mantenida anteriormente.)
@param(teclas Es una cadena de texto donde se encuentra la secuencia de teclas a
emular separadas por el signo +.)
@param(codigo Representa el código de la tecla pulsada que no es una tecla de
control, para emular correctamente su pulsación en el sistema operativo.)   }
procedure ejecutar_teclas(tipo: string; teclas: string; codigo: word);
{Se encarga de crear o cargar un archivo .ini que contiene el perfil que se
carga por defecto al arrancar la aplicación.
@param(cargar Si está en @true, carga el archivo .ini y el perfil indicado en
éste, si está en @false se crea o guarda el archivo .ini.)
@param(ruta_guardado Contiene la ruta del perfil que se quiere cargar al arrancar
la aplicación. Sólo se tiene en cuenta si cargar es @false, puesto que se va a
guardar. Por defecto es cadena vacía.)}
procedure perfil_por_def(cargar: bool; ruta_guardado: string = '');
{Realiza la misma función que SerialRxData, pero con el puerto USB.}
procedure datos_usb_rec();

var
  Form1: TForm1;          //< Formulario principal de la aplicación.
  perfil: TIniFile;       //< Contiene una copia en memoria del perfil.
  modo_actual: TModo;     //< Modo actual en el que se encuentra la aplicación.
  { Indica si el modo avance/retroceso de los rotatorios está o no activado.}
  modo_av_ret_rotatorios: bool = False;
  {Indica si es panel está activo y puede mandar teclas.}
  panel_activo: bool = True;
  posRotA: word = $A0;    //< Posición actual del rotatorio A.
  posRotC: word = $C0;    //< Posición actual del rotatorio C.
  posRotD: word = $D0;    //< Posición actual del rotatorio D.
  {Tiempo en el que se capturó el último comando en modo edición.}
  tiempo: TDateTime;
  CP3_USB: TCpanel;         //< Dispositivo USB para comunicación con el panel.
  trama_usb: TBytesRec;  //< Se guardan los datos recibidos por USB.
  trama_usb_lista: Boolean = False; //< Indica que la escritura a terminado.


implementation

uses
  captura;

{$R *.lfm}


{ TForm1 }


procedure TForm1.btnConfigClick(Sender: TObject);
begin
  Serial.ShowSetupDialog; // Abre el diálogo para la configuración del puerto serie.
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  {@exclude Inicialización de objetos.}
  modo_actual := normal;
  lblPerfil2.Caption := 'Ninguno';
  lblcon2.Caption := 'Desconec.';
  lblcon2.Font.Color := clRed;
  lblModo2.Caption := 'Normal';
  perfil_por_def(True);
  tiempo := Time;
end;

procedure perfil_por_def(cargar: bool; ruta_guardado: string = '');
var
  ruta: string;
  ruta_perfil_def: string;
  initfile: TIniFile;
begin

  ruta := ExtractFilePath(Application.ExeName) + 'perfil_inicio.ini';

  try
    initfile := TIniFile.Create(ruta);
    if (FileExistsUTF8(ruta)) and (cargar = True) then
    begin
      ruta_perfil_def := initfile.ReadString('PERFIL', 'Por_defecto', '');
      if (ruta_perfil_def <> '') then
        carga_perfil(ruta_perfil_def);
    end;

    if (cargar = False) and (ruta_guardado <> '') then
      // Se guarda el perfil como perfil por defecto.
      initfile.WriteString('PERFIL', 'Por_defecto', ruta_guardado);

    initfile.Free;
  except
    on E: Exception do
    begin
      initfile.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;
  end;

end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  if Assigned(perfil) then
    FreeAndNil(perfil);

  tmrDatosRecibidos.Enabled := False;

  desconectar_panel();

end;



function teclado_pulsar(Comando: string): integer;
var
  teclas: string;
  tipo: string;
  punto: string;
  codigo: word = 0;
  i: integer = 0;
begin

  //Sleep(20);      // Descomentar si hay problemas con la conexión serie.
  try
    if (Assigned(perfil) = False) then
    begin
      MessageDlg('Error', 'No hay ningún perfil seleccionado', mtError, [mbOK], 0);
      Exit(1);
    end
    else
    begin
      if (Comando = '0B460') then  // Pulsador del joystick analógico.
      begin
        if (panel_activo) then
          panel_activo := False     // Desactivo el panel para no mandar teclas.
        else
        begin
          panel_activo := True;
          Exit(1);               // Para no habilitar la tecla para otra función.
        end;
      end;

      if (panel_activo = False) or (Comando = '0B461') or (Comando = '000E7') then
        Exit(1);

      if (modo_actual = normal) then
      begin
        if (cargar_teclas(Comando, tipo, punto, teclas, codigo)) then
          ejecutar_teclas(tipo, teclas, codigo);
      end;


      if (modo_actual = prueba) then
      begin
        if (cargar_teclas(Comando, tipo, punto, teclas, codigo)) then
        begin
          // Muestra el punto rojo del botón/interruptor accionado durante 500 ms.
          for i := 0 to Form1.Panel1.ControlCount - 1 do
          begin
            if (Form1.Panel1.Controls[i].Name = punto) then
            begin
              Form1.Panel1.Controls[i].Visible := True;
              Form1.Refresh;
              Sleep(500);
              Form1.Panel1.Controls[i].Visible := False;
              Break;
            end;
          end;
        end;
      end;


      if (modo_actual = edicion) then
      begin

        {@exclude Solo captura un comando para su edición si han pasado 80 ms o más desde
        el anterior comando, de este modo se pueden pulsar rápidamente los
        botones para obtener el código asociado a la pulsación y no el asociado
        al soltar el botón.}

        if (MilliSecondsBetween(Now, tiempo) < 80) then
          Exit(1);

        frmCaptura.Close;
        frmCaptura.Show;
        frmCaptura.Comando := Comando;
        frmCaptura.lblComando2.Caption := Comando;

        if (modo_av_ret_rotatorios) then
          frmCaptura.ckbRotatorios.Checked := True
        else
          frmCaptura.ckbRotatorios.Checked := False;


        if (cargar_teclas(Comando, tipo, punto, teclas, codigo, True) = false ) then
        begin
          frmCaptura.cmbTipo.Caption := 'Pulsar';
          frmCaptura.lblPunto2.Caption := '';
          frmCaptura.lblTeclas2.Caption := '';
          frmCaptura.lblCodigo.Caption := '';
        end;

        tiempo := Now;      // Actualiza el tiempo del último comando capturado.
      end;
    end;

  except
    on E: Exception do
    begin
      perfil.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
      form1.lblPerfil2.Caption := 'Ninguno';
    end;
  end;
end;

function cargar_teclas(var Comando: string; var tipo: string; var punto: string;
  var teclas: string; var codigo: word; mostrar: boolean = False):Boolean;
var
  codigoStr: string;
  valor: word;
  code: word;
  cmd_rotatorios: array [0..14] of
  string = ('000A0', '000A1', '000A2', '000A3', '000C0', '000C1',
    '000C2', '000C3', '000C4', '000C5', '000D0', '000D1', '000D2', '000D3', '000D4');
  i: integer;
  es_rotatorio: bool = False;
begin

  if (modo_av_ret_rotatorios) then
  begin
    for i := 0 to High(cmd_rotatorios) do
    begin
      if (Comando = cmd_rotatorios[i]) then
      begin
        es_rotatorio := True;
        Break;
      end;
    end;

    if (es_rotatorio) then
    begin
      Val('0x' + Comando, valor, code); // Paso el comando a número hexadecimal.
      if (code = 0) then
      begin
        if (i in [0..3]) then         // Es el rotatorio A.
        begin
          if (valor > posRotA) then   // Giro a derechas.
            Comando := '000A1'
          else                        // Giro a izquierdas.
            Comando := '000A0';

          posRotA := valor;          // Se actualiza la posición.
        end;

        if (i in [4..9]) then        // Es el rotatorio C.
        begin
          if (valor > posRotC) then
            Comando := '000C1'
          else
            Comando := '000C0';

          posRotC := valor;
        end;

        if (i in [10..14]) then      // Es el rotatorio D.
        begin
          if (valor > posRotD) then
            Comando := '000D1'
          else
            Comando := '000D0';

          posRotD := valor;
        end;
      end;

      if (modo_actual = edicion) then
      begin
        frmCaptura.lblComando2.Caption := Comando;
        frmCaptura.Comando := Comando;
      end;

    end;
  end;

  tipo := perfil.ReadString(Comando, 'tipo', '');
  punto := perfil.ReadString(Comando, 'punto', '');
  teclas := perfil.ReadString(Comando, 'teclas', '');
  codigoStr := perfil.ReadString(Comando, 'codigo', '');

  if tipo = '' then
    Exit(false);

  if (codigoStr <> '') then
  begin
    Val(codigoStr, valor, code);
    if (code = 0) then
      codigo := valor
    else
      codigo := 0;
  end
  else
    codigo := 0;

  if (mostrar) then
  begin
    frmCaptura.cmbTipo.Caption := tipo;
    frmCaptura.lblPunto2.Caption := punto;
    frmCaptura.lblTeclas2.Caption := teclas;
    frmCaptura.lblCodigo.Caption := codigoStr;
  end;

  Exit(true);
end;

procedure ejecutar_teclas(tipo: string; teclas: string; codigo: word);
var
  miTeclado: Tteclado;
begin

  if (codigo <> 0) then
  begin

    miTeclado := Tteclado.Create;

    if (Pos('Shift', teclas) <> 0) then
      miTeclado.pulsar(VK_SHIFT);

    if (Pos('Ctrl', teclas) <> 0) then
      miTeclado.pulsar(VK_CONTROL);

    if (Pos('Alt', teclas) <> 0) then
      miTeclado.pulsar(VK_MENU);


    if tipo = 'Pulsar' then
    begin
      miTeclado.pulsar(codigo);
      Sleep(10);
      miTeclado.soltar(codigo);
    end;

    if tipo = 'Mantener' then
      miTeclado.pulsar(codigo);

    if tipo = 'Soltar' then
      miTeclado.soltar(codigo);


    if (Pos('Alt', teclas) <> 0) then
      miTeclado.soltar(VK_MENU);

    if (Pos('Ctrl', teclas) <> 0) then
      miTeclado.soltar(VK_CONTROL);

    if (Pos('Shift', teclas) <> 0) then
      miTeclado.soltar(VK_SHIFT);

    miTeclado.Free;
  end;
end;


procedure TForm1.btnCargarClick(Sender: TObject);
var
  nombre_archivo: string;
  rest: integer;
begin
  if (OpenDlg.Execute) then
  begin
    nombre_archivo := OpenDlg.FileName;
    if (FileExistsUTF8(nombre_archivo)) then
      carga_perfil(nombre_archivo)
    else
    begin
      rest := MessageDlg('El perfil no existe', '¿Desea crear un perfil nuevo?',
        mtInformation, mbYesNo, 0);

      if (rest = mrNo) then
        lblPerfil2.Caption := 'Ninguno'
      else
        crear_perfil(nombre_archivo);
    end;
  end;

end;

procedure TForm1.rgConexionSelectionChange(Sender: TObject);
begin
  try
    begin
      if (rgConexion.Items[0].Checked) then
      begin
        lblcon2.Caption := 'Conectando';
        lblcon2.Font.Color := clBlue;
        Refresh;
        conectar_panel();
      end
      else
      begin
        tmrDatosRecibidos.Enabled := False;
        desconectar_panel();
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Error en conumicación', E.Message, mtError, [mbOK], 0);
      lblcon2.Caption := 'Desconec.';
      lblcon2.Font.Color := clRed;
    end;
  end;
end;

procedure TForm1.rgModoSelectionChange(Sender: TObject);
begin

  if (Form1.rgModo.Items[0].Checked) then    // Modo Edición
  begin
    modo_actual := edicion;
    lblModo2.Caption := 'Edición';
  end;


  if (Form1.rgModo.Items[1].Checked) then    // Modo Prueba
  begin
    modo_actual := prueba;
    lblModo2.Caption := 'Prueba';
  end;

  if (Form1.rgModo.Items[2].Checked) then    // Modo Normal
  begin
    modo_actual := normal;
    lblModo2.Caption := 'Normal';
  end;

end;


procedure CRC16_Update(var CRC: word; Data: byte);
var
  I: integer;
begin
  CRC := CRC xor (word(Data));
  for I := 0 to 7 do
  begin
    if (CRC and 1) <> 0 then
      CRC := (CRC shr 1) xor $A001
    else
      CRC := CRC shr 1;
  end;
end;


procedure carga_perfil(ruta: string);
var
  puerto: string;
  modo_rotatorios: string;
begin
  try
    perfil := TIniFile.Create(ruta);
    puerto := perfil.ReadString('PUERTO', 'valor', 'COM1');
    modo_rotatorios := perfil.ReadString('ROTATORIOS', 'Modo_av/ret', 'Desactivado');

    if (modo_rotatorios = 'Activado') then
      modo_av_ret_rotatorios := True
    else
      modo_av_ret_rotatorios := False;

    form1.Serial.Device := puerto;
    form1.lblPerfil2.Caption := ExtractFileNameOnly(ruta);
    perfil_por_def(False, ruta);

  except
    on E: Exception do
    begin
      perfil.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
      form1.lblPerfil2.Caption := 'Ninguno';
    end;

  end;
end;

procedure crear_perfil(ruta: string);
var
  ini: TIniFile;
  F: longint;
begin

  try
    F := FileCreateUTF8(ruta);
    FileClose(F);
    ini := TIniFile.Create(ruta);
    ini.WriteString('PUERTO', 'valor', Form1.Serial.Device);
    ini.WriteString('ROTATORIOS', 'Modo_av/ret', 'Desactivado');
    ini.Free;
    carga_perfil(ruta);

  except
    on E: Exception do
    begin
      ini.Free;
      MessageDlg('Error', E.Message, mtError, [mbOK], 0);
    end;

  end;

end;


procedure preparar_trama(var trama_buffer: TBytesRec; Comando: cardinal);
type
  Tcmd = array [0..2] of byte;
var
  cmd: Tcmd;
  crc: word = 0;
  i: byte = 0;
begin
  for i := 0 to 2 do
    cmd[i] := 0;

  cmd[0] := byte(Comando);
  cmd[1] := byte(Comando shr 8);
  cmd[2] := byte(Comando shr 16);

  for i := 0 to 2 do
    CRC16_Update(crc, cmd[i]);

  trama_buffer[0] := $7e;             // Byte de sincronización
  trama_buffer[1] := cmd[2];          // Interruptor MSB primero 3 Bytes
  trama_buffer[2] := cmd[1];
  trama_buffer[3] := cmd[0];
  trama_buffer[4] := byte(crc shr 8); // Crc MSB
  trama_buffer[5] := byte(crc);       // Crc LSB

end;


function leer_trama(const trama_buffer: TBytesRec): cardinal;
type
  Tcmd = array [0..2] of byte;
var
  cmd: Tcmd;
  crc_trama: word = 0;
  crc_calculado: word = 0;
  Comando: cardinal = 0;
  i: byte = 0;
begin
  for i := 0 to 2 do
    cmd[i] := 0;

  if (trama_buffer[0] = $7e) then
  begin
    Comando := (trama_buffer[1] shl 16) or (trama_buffer[2] shl 8) or
      (trama_buffer[3]);

    crc_trama := (trama_buffer[4] shl 8) or (trama_buffer[5]);

    cmd[0] := byte(Comando);
    cmd[1] := byte(Comando shr 8);
    cmd[2] := byte(Comando shr 16);

    for i := 0 to 2 do
      CRC16_Update(crc_calculado, cmd[i]);

    if (crc_trama = crc_calculado) then  // Trama válida
      Exit(Comando)
    else                         // Crc erróneo
      Exit(0);
  end
  else
    Exit(0);                  // Sin byte de sincronización, trama nó válida

end;

procedure datos_usb_rec();
var
   Comando: cardinal = 0;
   tx_buffer: TBytesRec;
   i: byte = 0;
begin
  for i := 0 to 5 do
  begin
    tx_buffer[i] := 0;
  end;

  tx_buffer[0] := $aa; // Indica que se han leido los datos.
  Comando := leer_trama(trama_usb);
  CP3_USB.mandar_trama_USB(tx_buffer);
  trama_usb_lista := false;

  if (Comando = 0) then
  begin
    Sleep(100);
    preparar_trama(tx_buffer, $e7);
    CP3_USB.mandar_trama_USB(tx_buffer);  // Realiza petición de reenvío.
  end
  else
  begin
    teclado_pulsar(IntToHex(Comando, 5));
  end;

end;

procedure TForm1.conectar_panel();
begin
  if (not Assigned(CP3_USB)) then
  begin
    CP3_USB := TCpanel.Create();

    if (CP3_USB.AProcess.Running) then
    begin
      //Serial.Active := True;
      lblcon2.Caption := 'Conectado';
      lblcon2.Font.Color := clGreen;
      tmrDatosRecibidos.Enabled := True;
     // if Assigned(perfil) then
    //    perfil.WriteString('PUERTO', 'valor', Form1.Serial.Device);
    end;
  end;
end;

procedure TForm1.desconectar_panel();
var
   buffer: TBytesRec;
begin
  if (Assigned(CP3_USB)) then
  begin
    try
      buffer[0] := $cc;      // Se manda la orden de cierre del programa servidor.
      CP3_USB.mandar_trama_USB(buffer);
      FreeAndNil(CP3_USB);
    except
      CP3_USB := nil;
    end;

    //Serial.Active := False;
    lblcon2.Caption := 'Desconec.';
    lblcon2.Font.Color := clRed;
  end;
end;

procedure TForm1.SerialRxData(Sender: TObject);
//var
//  BytesRecibidos: TBytesRec;
//  BytesTemp: TBytesRec;
//  tx_buffer: TBytesRec;
//  BytesLeidos: integer = 0;
//  Comando: cardinal = 0;
//  i: byte = 0;
begin
  //for i := 0 to 5 do
  //begin
  //  BytesRecibidos[i] := 0;
  //  tx_buffer[i] := 0;
  //  BytesTemp[i] := 0;
  //end;
  //
  //Serial.ReadBuffer(BytesRecibidos, 1);
  //
  //if (BytesRecibidos[0] <> 0) then
  //begin
  //
  //  BytesLeidos := Serial.ReadBuffer(BytesTemp, 5);
  //  for i := 0 to 4 do
  //    BytesRecibidos[i+1] := BytesTemp[i];
  //
  //  if (BytesLeidos = 5) then
  //  begin
  //    Comando := leer_trama(BytesRecibidos);
  //    if (Comando = 0) then
  //    begin
  //      preparar_trama(tx_buffer, $e7);
  //      Serial.WriteBuffer(tx_buffer, 6);   // Realiza petición de reenvío.
  //    end
  //    else
  //    begin
  //      teclado_pulsar(IntToHex(Comando, 5));
  //    end;
  //
  //  end;
  //end;
end;

procedure TForm1.sp1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: integer; MousePos: TPoint; var Handled: boolean);
begin
  if (modo_actual = edicion) and (frmCaptura.Visible = True) then
    frmCaptura.lblPunto2.Caption := (Sender as TShape).Name;
end;

procedure TForm1.tmrDatosRecibidosTimer(Sender: TObject);
begin
   // Comprueba que la placa sigue conectada.
  if (CP3_USB.AProcess.Running = False) then
  begin
    desconectar_panel();      // Si no, intenta reconectar
    conectar_panel();
  end
  else
  begin
    // Comprueba si hay datos USB listos para procesar.
    if (trama_usb_lista) then
      datos_usb_rec();
  end;
end;


end.
