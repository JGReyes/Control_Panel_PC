{/*
*  cpanel
*
*  Created on: 22/11/2016
*      Author: JGReyes (www.circuiteando.net)
*
*  License: GPLv3 (www.gnu.org/licenses/gpl.html)
*/
}

unit cpanel;


interface

uses
  SysUtils, Classes, Sockets, process;

type

  TBytesRec = array [0..5] of byte;

  { TCpanel }

  TCpanel=class(TThread)
  private
    direccion: String;             //< Dirección IP.
    puerto: Word;                  //< Puerto para la conexión TCP.
    buffer_entrada: TBytesRec;     //< Almacena los datos recibidos por USB.
    buffer_salida: TBytesRec;      //< Almacena los datos a mandar por USB.
    in_Socket: LongInt;            //< Identificador del socket de entrada
    out_Socket: LongInt;           //< Identificador del socket de salida.
    conectado: Boolean;            //< Indica si se han conectado ambos sockets.
    {Se encarga de conectar los sockets.
    @param(output_socket Si es @true solo conecta el socket de salida,
    de lo contrario conecta ambos sockets.)
    @returns(@true si la conexión de ambos socket fue realizada,
    @false en caso contrario.)}
    function conectar(output_socket: Boolean): Boolean;
    {Se mantiene a la escucha de datos por parte del panel
    y notifica la llegada de datos al hilo principal.}
    procedure Execute; override;
  public
    AProcess: TProcess; //< Contiene el proceso a lanzar en python.
    {Inizializa el objeto cpanel para su conexión.
    @param(addr Dirección IP del servidor.)
    @param(port Puerto del servidor.)}
    constructor Create(addr: string = '127.0.0.1'; port: Word = 12345);
    destructor Destroy;override;
    {Se encarga de mandar los datos al panel.
    @param(Trama_usb es el array de bytes a mandar.)
    @returns(@true si se realizó correctamente, @false en caso contrario)}
    function mandar_trama_USB(const trama_usb: TBytesRec):Boolean;


  end;


implementation

uses
  main, Graphics;

function TCpanel.mandar_trama_USB(const trama_usb: TBytesRec):Boolean;
var
  i: Integer = 0;
  pbuffer: ^TBytesRec;
begin

  pbuffer := @buffer_salida;
  if (conectado) then
  begin
    for i := Low(trama_usb) to High(trama_usb) do
    begin
      buffer_salida[i] := trama_usb[i];
    end;

    if (fpsend(out_Socket, pbuffer, sizeOf(buffer_salida), 1) <> -1) then
      Result := True
    else
      Result := False;
  end
  else
    Result := False;

end;

constructor TCpanel.Create(addr: string = '127.0.0.1'; port: Word = 12345);
var
  reintentos: Word = 0;
begin
  inherited Create(True);

  direccion := addr;
  puerto := port;

   // Ejecuta el script de python en un proceso.
   AProcess := TProcess.Create(nil);
   AProcess.Executable := 'CPV3_USB_Server.exe';
   //AProcess.Executable := 'python';
   //AProcess.Parameters.Add('CPV3_USB_Server.py');
   AProcess.Parameters.Add(direccion);
   AProcess.Parameters.Add(IntToStr(puerto));
   AProcess.Execute;

   // Intenta reconectar por 5 segundos.
   while (conectar(False) = False) do
   begin
     Inc(reintentos);
     Sleep(500);
     if (reintentos = 10) then
        Break;
   end;

   if (conectado) then
     Self.Start          // Inicia el hilo de recepción de datos.
   else
   begin
     Form1.lblcon2.Caption := 'Desconec.';
     Form1.lblcon2.Font.Color := clRed;
     if Assigned(AProcess) then
     begin
       AProcess.Terminate(0);
       AProcess.Free;
     end;
   end;
end;

function TCpanel.conectar(output_socket:Boolean): Boolean;
var
  saddr: TSockAddr;
  saddrb: TSockAddr;
  psaddr: psockaddr;
  psaddrb: psockaddr;
  in_socket_con: Boolean = False;
  out_socket_con: Boolean = False;
begin

   // Inicia el socket de entrada.
  if (output_socket = False) then
  begin
    in_Socket := fpsocket(AF_INET, SOCK_STREAM, 0);
    if (in_Socket <> -1) then
    begin
      saddr.sin_family := AF_INET;
      saddr.sa_family := AF_INET;
      saddr.sin_port := htons(puerto);
      saddr.sin_addr := StrToNetAddr(direccion);
      psaddr := @saddr;

       if (fpconnect(in_Socket, psaddr, sizeOf(saddr)) <> -1) then
       begin
         in_socket_con := True ;
       end;
    end;
  end;

  Sleep(100);

  // Inicia el socket de salida.
  if (True) then
  begin
    out_Socket := fpsocket(AF_INET, SOCK_STREAM, 0);
    if (out_Socket <> -1) then
    begin
      saddrb.sin_family := AF_INET;
      saddrb.sa_family := AF_INET;
      saddrb.sin_port := htons(puerto + 1);
      saddrb.sin_addr := StrToNetAddr(direccion);
      psaddrb := @saddrb;

       if (fpconnect(out_Socket, psaddrb, sizeOf(saddrb)) <> -1) then
       begin
         out_socket_con := True ;
       end;
    end;
  end;

  if (in_socket_con and out_socket_con) then
  begin
    conectado := True;
    Result := True;
  end
  else
    Result := False;

end;

procedure TCpanel.Execute();
var
  pbuffer: ^TBytesRec;
  i: Integer = 0;
begin
  pbuffer := @buffer_entrada;
  while (conectado) do
  begin
    if ((fprecv(in_Socket, pbuffer, 6, 0) <> -1)) then   // A la espera de datos.
    begin
      if (buffer_entrada[0] = $a1) then   // Se pide reconectar el socket.
      begin
        Sleep(100);
        conectar(True);
        Continue;
      end;

      // Si son datos de un comando se cargan en el buffer
      for i := Low(trama_usb) to High(trama_usb) do
      begin
        trama_usb[i] := buffer_entrada[i];
      end;
      trama_usb_lista := True;
    end
    else
    begin
      conectado := False;
      fpshutdown(in_Socket, 2);
      fpshutdown(out_Socket, 2);
    end;
  end;
end;

destructor TCpanel.Destroy;
begin

  if Assigned(AProcess) then
  begin
    AProcess.Terminate(0);    // Termina con el proceso de python.
    AProcess.Free;
  end;

  if (in_Socket <> 0) then
    CloseSocket(in_Socket);

  if (out_Socket <>0) then
    CloseSocket(out_Socket);

  inherited Destroy;
end;


end.

