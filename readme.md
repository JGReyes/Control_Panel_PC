El programa de PC se encarga de recibir los datos del panel de control y emular un teclado. Pudiendo crear/lanzar distintos perfiles.

![Programa_PC](https://4.bp.blogspot.com/-hiNSS6-ehqE/WyQJMs289tI/AAAAAAAAAKs/jxjd3CN28RYPfbOtxud9BjWlJO_G95HCgCKgBGAs/s640/ControlPanelV3.jpg)

Las características del programa son las siguientes:

- Realiza comprobación de errores (CRC 16) en la comunicación.
- Emula secuencias de teclas mediante scancodes.
- Capturar pulsaciones del teclado y traducirlas a scancodes.
- Crear y modificar perfiles de teclas.
- Cargar automáticamente el último perfil utilizado.
- Informar del estado actual del panel y modo de ejecución.
- Poder probar los perfiles y controles para asegurar su correcto funcionamiento.
- Recuperación de errores mediante reinicio automático de procesos.
- Lectura y envío de informes USB HID.

Para más información consultar el [blog.](https://www.circuiteando.net/2018/06/panel-de-control-parte-6-software.html)